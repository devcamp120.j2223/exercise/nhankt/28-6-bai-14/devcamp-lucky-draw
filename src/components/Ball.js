const { Component } = require("react");

class Ball extends Component{
    constructor(props){
        super(props)
        this.state = {numbers : []}
    }
    onBtnClick = ()=>{
        let newNumber = []
        for (let i =0 ; i<6; i++){
            newNumber.push(Math.floor(Math.random() * 99)+1)
        }
        this.setState({numbers : newNumber})
    }
    render(){
        let i = 0;
        return(
        <div>
            <div>
            {this.state.numbers.map((number)=>{
                return(<button key={++i} style={{backgroundColor:"orange", borderRadius:"50%", marginRight:"4rem", minWidth:"60px", minHeight:"60px"}}>{number}</button>)  
            })}
            </div>
            <button style={{marginLeft:"17rem", marginTop:"3rem", backgroundColor:"purple", height:"3rem", color:"white"}} onClick={this.onBtnClick}>Generate</button>
        </div>
        )
    }
}
export default Ball