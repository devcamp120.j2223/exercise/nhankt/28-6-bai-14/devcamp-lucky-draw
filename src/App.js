import logo from './logo.svg';
import './App.css';
import Ball from './components/Ball';

function App() {
  return (
    <div>
      <h1>Lucky Draw</h1>
      <Ball></Ball>
    </div>
  );
}

export default App;
